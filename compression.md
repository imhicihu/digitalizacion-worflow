## Presets ##

* Medium compression

    - Image quality: 75%
    - Maximum resolution: 144 dpi
    - Color profile: Original
    - ✔︎ Reduce the color complexity of images
    - ✔︎ Clip invisible parts of images
    - ✔︎ Remove alternate images
    - ✘ Force image recompression
    - ✘ Remove all images
    - ![image.png](https://bitbucket.org/repo/5qA7gpA/images/2674561997-compressing.png)