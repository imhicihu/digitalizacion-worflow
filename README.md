![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internal-wip](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# Rationale #

* Workflow and digital strategies describing the process to migrate analog data to the digital realm

![bibliography.gif](https://i.ibb.co/Lnrd0y7/bibliography.gif)

* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

* Quick summary
     - Good practices plus some workflow enhancements. Essentially, this repo is a massive [_checklist_](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/master/Checklist.md).
![praehistoria.png](https://bitbucket.org/repo/5qA7gpA/images/2517272857-praehistoria.png)

### How do I get set up? ###

* Summary of set up
     - Verify [checklist.md](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/master/Checklist.md)
     - Check [Best practices.md](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/master/Best_practices.md)
* Configuration
     - Check [Colophon.md](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/master/Colophon.md?at=master)
* Dependencies
     - There is no dependencies.

### Related repositories ###

* Some repositories linked with this project:
     - [Incunnabilia (early book)- Digitization](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/src/)
     - [PDF inner structure](https://bitbucket.org/imhicihu/pdf-inner-structure/src/master/src/)
     - [AI Document recognition](https://bitbucket.org/imhicihu/ai-document-recognition/src/)
     - [Migration data](https://bitbucket.org/imhicihu/migration-data-checklist/src/)
     
### Who do I talk to? ###

* Repo owner or admin
     - Contact to `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Contact is enable on the [board](https://bitbucket.org/imhicihu/digitalizacion-worflow/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account).
     
### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/digitalizacion-worflow/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/digitalizacion-worflow/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/digitalizacion-worflow/commits/) section for the current status

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.     

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png) 